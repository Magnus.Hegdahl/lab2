package INF101.lab2;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.TreeMap;

public class Fridge implements IFridge {

  static private final int capacity = 20;
  private int itemCount;

  private TreeMap<LocalDate, TreeMap<String, Integer>> content;

  public Fridge() {
    itemCount = 0;
    content = new TreeMap<>();
  }

  public int nItemsInFridge() {
    return itemCount;
  }

  public int totalSize() {
    return capacity;
  }

  public boolean placeIn(FridgeItem item) {
    if (nItemsInFridge() == totalSize())
      return false;

    if (!content.containsKey(item.getExpirationDate()))
      content.put(item.getExpirationDate(), new TreeMap<>());

    ++itemCount;

    int count = content
      .get(item.getExpirationDate())
      .getOrDefault(item.getName(), 0);

    ++count;

    content
      .get(item.getExpirationDate())
      .put(item.getName(), count);

    return true;
  }

  public void takeOut(FridgeItem item) {
    --itemCount;

    if (!content
        .containsKey(item.getExpirationDate()))
      throw new NoSuchElementException();
    if (!content
        .get(item.getExpirationDate())
        .containsKey(item.getName()))
      throw new NoSuchElementException();

    Integer count = content
      .get(item.getExpirationDate())
      .getOrDefault(item.getName(), 0);

    --count;

    if (count > 0) {
      content
        .get(item.getExpirationDate())
        .put(item.getName(), count);
    } else {
      content
        .get(item.getExpirationDate())
        .remove(item.getName());
      if (content
          .get(item.getExpirationDate())
          .isEmpty())
        content.remove(item.getExpirationDate());
    }
  }

  public void emptyFridge() {
    itemCount = 0;
    content.clear();
  }

  public List<FridgeItem> removeExpiredFood() {
    LinkedList<FridgeItem> removed = new LinkedList<>();

    FridgeItem earliest = earliestExpiration();
    while (earliest != null && earliest.hasExpired()) {
      removed.add(earliest);
      takeOut(earliest);
      earliest = earliestExpiration();
    }

    System.out.println(removed);

    return removed;
  }

  private FridgeItem earliestExpiration() {
    if (nItemsInFridge() == 0)
      return null;

    LocalDate date = content.firstKey();
    String name = content.get(date).firstKey();
    return new FridgeItem(name, date);
  }

}
